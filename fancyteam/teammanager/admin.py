from django.contrib import admin

# Register your models here.

from .models import *

class ChildTeamInline(admin.TabularInline):
    model = Team

class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'team_type', 'level', 'created_at', 'modified_at', 'parent')
    inlines = [
        ChildTeamInline,
    ]

admin.site.register(Team, TeamAdmin)
