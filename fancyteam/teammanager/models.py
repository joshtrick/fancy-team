from django.db import models

# Create your models here.
import datetime
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, MaxValueValidator

class Team(models.Model):

    class TeamType(models.TextChoices):
        BG  = 'BG', _('Business Group')
        BU  = 'BU', _('Business Unit')
        DPT = 'DPT', _('Department')
        GRP = 'GRP', _('Group')
        LAB = 'LAB', _('Laboratory')
        UDT = 'UDT', _('Undefined Team')

    name            = models.CharField(max_length=200)
    created_at      = models.DateTimeField(auto_now_add=True)
    modified_at     = models.DateTimeField(auto_now=True)
    team_type       = models.CharField(
                        max_length=200,
                        choices=TeamType.choices,
                        default=TeamType.UDT,
                    )
    level           = models.IntegerField(
                        validators=[
                            MinValueValidator(1),
                            MaxValueValidator(10),
                            ],
                        default=1,
                    )
    parent          = models.ForeignKey(
                        'self',
                        models.PROTECT,
                        blank=True,
                        null=True,
                    )
    @property
    def full_name(self):
        return "%s %s" % (self.name, self.TeamType(self.team_type).label)

    def list_from_header_to_this_team(self):
        print(self)
        tmp_pk = self.pk
        while Team.objects.get(pk=tmp_pk).parent:
            tmp_pk = Team.objects.get(pk=tmp_pk).parent.pk
            print('v')
            print(Team.objects.get(pk=tmp_pk))

    def clean(self):
        super().clean()
        if self.parent:
            if self.parent.pk == self.pk:
                raise ValidationError({
                    'parent': ValidationError(_("Parent team CANNOT be this team"), code='errors'),
                }, code='errors')
            elif self.parent.level >= self.level:
                raise ValidationError({
                    'parent': ValidationError(_("Ensure this value is greater than the parent level"), code='errors'),
                    'level': ValidationError(_("The parent level CANNOT be greater than or equal to this team level"), code='errors'),
                }, code='errors')

        for child_team in self.team_set.all():
            if child_team.level <= self.level:
                raise ValidationError({
                    'level': ValidationError(_("Ensure this value is less than all children's level"), code='errors'),
                }, code='errors')

        for team in Team.objects.all():
            if self.pk != team.pk and self.full_name == team.full_name:
                if (self.parent and team.parent and self.parent.pk == team.parent.pk) or (not self.parent and not team.parent):
                    raise ValidationError({
                        'parent': ValidationError(_("No identical teams are allowed in the same parent team"), code='errors'),
                    }, code='errors')

    def __str__(self):
        return "[Level %d] %s of %s" % (self.level, self.TeamType(self.team_type).label, self.name)
